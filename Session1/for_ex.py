#Add the numbers smaller than a given number

def add_them_up(max):
    sum = 0
    numbers = range(max)
    for number in numbers:
        sum += number
    return sum


print add_them_up(4)

#Modify the function to add the number smaller or equal to the given number
#Modify the function to add only odd numbers in the sum