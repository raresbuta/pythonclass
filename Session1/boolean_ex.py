#Boolean Data Type - used in conditionals, while loops, flags
a = True
b = False
print a and b
print a or b
print a + b

#None
x = None
y = []
z = ''

print y
print not y
print z
print not z
